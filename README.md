### So, this is happened. Head to my website (in my profile), to track where I will upload this repositories.

**Note to GitLab:** DMCA is "legal" only in *some* countries. Disclosing ip, name and any other uniq identifier is illegal by Russian law. Be aware, that you *will* be sued and your web-resource will be blocked.

**Note to Nintendo:** if you try to DMCA this repos, I will upload this code ounto .ru domains; I'm from Russia, DMCA has no legal force here. You left our country for good, and our laws no longer protects you. "Torrenting" your games are also no longer "illigal". Also, because of USA hybreed war agains Russia, you can't do shit about me (see law about personal data in Russia).

I'm have my hands on some interesting documents about Yuzu-Nintendo conflict, but not sure, if I must publish it.

I have all repositories locally, of course. You can't stop me.

**Note to who will find this:** do a forks, do a clones, do a website [wayback machine clones](https://archive.org). Don't be afraid. Today Nintendo closed **open source** emulator, tomorrow you [will lost access to your favorite Linux distrubution](https://arstechnica.com/gadgets/2021/05/fake-dmca-takedown-notice-targeted-ubuntu-downloaders-yesterday/) or worse - your right to [repair hardware](https://www.repair.org/stand-up/) or your right to be [an owner of the hardware you've buying](https://www.howtogeek.com/125664/you-paid-for-your-tech-but-you-dont-really-own-it/)!

`
The DMCA makes no distinctions about intent, ownership or even permission. Those are perhaps the biggest problems it has in the general case.

File permissions are rights management. The DMCA makes it very difficult to discuss anything that might compromise “rights management,” even if all you did was show that the products people were using are dangerously insecure. Right now, for example, it’s not clear that someone finding a rights management systems used by a friendly company was flawed without being prosecuted.

The advice given was pretty simple — chances of being arrested non-zero; chance of being convicted by a sane U.S. court — nil. However, spending six months trapped in the U.S.A. does not appeal to me.

- Alan Cox, former Linux contributor
`